variable "AWS_REGION" {
    default = "ap-south-1"
}
variable "secret" {
    default = ""
}

variable "access" {
    default = ""
}

variable "sg" {
    default = "sg-06e84409b2d14f457"
}

variable instance_data {
    default = {
        count = "2"
        ami = "ami-07ffb2f4d65357b42"
        type = "t2.micro"
    }
}