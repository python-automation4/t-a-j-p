import yaml
import os
from os.path import exists

# Reading no of public ip's
public_ips = []
data = yaml.safe_load(open("meta.yaml","rt").read())
resources = data["resources"]
resources_val = dict(resources[0])
instance = resources_val["instances"]
machine_count = len(instance)
print(machine_count)

# Capturing the public ip's to "public_ips"
for i in range(0,machine_count):
     instances_val = dict(instance[i])
     attributes = instances_val["attributes"]
     public_ips.append(attributes["public_ip"])

# Checking whether the hosts file in ansible folder exists for confirmation
abs_path="ansible/hosts.yaml"
file_exists = os.path.exists(abs_path)
print(file_exists)

# Writing the captured ip's to a host file in ansible folder
with open(abs_path) as file:
    contents = file.read()
    item = '[web]'
    if item in contents:
        file_write = ''
        for i in range(0, len(public_ips)):
            for ip in public_ips:
                if ip not in contents:  
                    file_write += ip + '\n'        
        ff = open(abs_path, "a")
        ff.write(file_write)
    else:
        file_write = item  
        file_write += '\n'
        for i in range(0, len(public_ips)):
            file_write += public_ips[i] + '\n'
        ff = open(abs_path, "a")
        ff.write(file_write)
