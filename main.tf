terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.74.0"
    }
  }
}

provider "aws" {
    region = var.AWS_REGION
    access_key = var.access
    secret_key = var.secret
}

resource "aws_instance" "apache_instance" {
    count= var.instance_data.count
    ami= var.instance_data.ami
    instance_type = var.instance_data.type
    key_name = "raghav"
    vpc_security_group_ids = [var.sg]
    tags = {
        Name  = "apache_server_${count.index}"
    }
}
