#!/usr/bin/python

# need to pip install pyhcl pyyaml
import sys, yaml, hcl

filename = sys.argv[1]

print("Reading", filename)
with open(filename, 'r') as fp:
        obj = hcl.load(fp)

print("Converting to yaml")
yamlfile = open('meta.yaml', 'w+')
conv = yaml.safe_dump(obj,yamlfile,default_flow_style=False)
print("Done.")
